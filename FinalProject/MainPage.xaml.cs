﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FinalProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        String base_url = "http://localhost:8080/kioskapi/public/v1/events/";
        public MainPage()
        {
            
            this.InitializeComponent();
            getData();
            
            
            

            
            /*String path = Directory.GetCurrentDirectory() + @"\Assets";
            flipViewImage.ItemsSource = Directory.GetFiles(path).Select(p => "ms-appx:///" + p);*/
            int change = 1;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += (o, a) =>
            {
                // If we'd go out of bounds then reverse
                int newIndex = flipViewImage.SelectedIndex + change;
                if (newIndex >= flipViewImage.Items.Count || newIndex < 0)
                {
                    change *= -1;
                }

                flipViewImage.SelectedIndex += change;
            };

            timer.Start();
        }

        private async void btnList_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AllEvent));
        }

        public async void getData()
        {
            HttpClient httpClient = new HttpClient();

            string url = base_url + "view-all";

            

            HttpResponseMessage response = await httpClient.GetAsync(url);

            string responseText = await response.Content.ReadAsStringAsync();
            JsonObject jsonObject = JsonValue.Parse(responseText).GetObject();
            string jsonString = jsonObject.ToString();

            var res = JsonConvert.DeserializeObject<Event>(jsonString);

            string message;
            if (res.success == "true")
            {
                message = "success!";

            }
            else
            {
                message = "fail";


            }
            var images = new List<BitmapImage>();
            var urlGambar="";
            for (var i = 0; i < res.data.Count; i++)
            {
                urlGambar = res.data[i].poster_path;
                var bitmap = new BitmapImage(new Uri(urlGambar, UriKind.Absolute));
                images.Add(bitmap);
                /*MessageDialog dialog1 = new MessageDialog( res.data[i].nama_event+ res.data[i].deskripsi);
                await dialog1.ShowAsync();*/
            }
            flipViewImage.ItemsSource = images;
            /*message += res.data[0].deskripsi;
            MessageDialog dialog = new MessageDialog(message);
            await dialog.ShowAsync();*/
        }

       

    }
}
