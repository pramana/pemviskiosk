﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    [DataContract]
    class Event
    {
        [DataMember]
        public string success { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<EventData> data { get; set; }
    }
}
