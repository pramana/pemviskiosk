﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    [DataContract]
    class EventData
    {
        [DataMember]
        public string id_event { get; set; }
        [DataMember]
        public string deskripsi { get; set; }
        [DataMember]
        public string nama_event { get; set; }
        [DataMember]
        public string poster_path { get; set; }
        [DataMember]
        public string penangungjawab { get; set; }
        [DataMember]
        public string tanggal_pelaksana { get; set; }
        [DataMember]
        public string himpunan { get; set; }
        [DataMember]
        public string created_at { get; set; }
        [DataMember]
        public string updated_at { get; set; }
    }
}
